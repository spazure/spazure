## Hi there 👋

![spazi](https://img.shields.io/static/v1?label=I%20am&message=spazure&color=9cf) ![wakatime](https://wakatime.com/badge/user/1beb5578-2dbc-4574-bdc0-fd327bdb8ba2.svg?text=code+time)

I'm a college student in school working towards a programming degree. I have worked in tech for over 20 years, but not as a developer. In recent years, I have decided to start the hard work of switching from someone who codes as a hobby to being a professional programmer.

[![Top Langs](https://github-readme-stats-spazure.vercel.app/api/top-langs/?username=spazure&count_private=true&hide=css&layout=compact)](https://github.com/anuraghazra/github-readme-stats)

#### Languages I'm currently learning:

![Swift](https://img.shields.io/badge/-Swift-orange?logo=swift&logoColor=white&style=flat) ![wakatime](https://wakatime.com/badge/user/1beb5578-2dbc-4574-bdc0-fd327bdb8ba2/project/968dbf64-941c-4169-bc18-0b31e631b0ee.svg)

 ![Python](https://img.shields.io/badge/Python-blue?logo=python&logoColor=white&style=flat) [![wakatime](https://wakatime.com/badge/user/1beb5578-2dbc-4574-bdc0-fd327bdb8ba2/project/cba6278e-8777-4219-b1d6-934949f04bf8.svg)](https://wakatime.com/badge/user/1beb5578-2dbc-4574-bdc0-fd327bdb8ba2/project/cba6278e-8777-4219-b1d6-934949f04bf8)




<!--
**spazure/spazure** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:


- 🔭 I’m currently working on ...
- 
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
